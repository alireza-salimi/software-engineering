from django.contrib import admin
from requests.models import *


class SIMAdmin(admin.ModelAdmin):
    list_display = ['number', 'first_name', 'last_name', 'national_code']


class BillAdmin(admin.ModelAdmin):
    list_display = ['type', 'cost', 'bill_id', 'payment_id']


class CarAdmin(admin.ModelAdmin):
    list_display = ['chassis_number', 'engine_number', 'plaque_number', 'city_code']


class MailAdmin(admin.ModelAdmin):
    list_display = ['tracking_code', 'sender_name', 'receiver_name']


class EducationalReceiptAdmin(admin.ModelAdmin):
    list_display = ['type', 'first_name', 'last_name']


class RequestAdmin(admin.ModelAdmin):
    list_display = ['type', 'status', 'date']


class ActivatingSIMCardRequestAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


class MinorConversationsAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


class ReceivingCarTollsAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


class TrackingMailAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


class ReceivingEducationalReceiptAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


class BillPaymentRequestAdmin(admin.ModelAdmin):
    list_display = ['model', 'user', 'type']


admin.site.register(BillPaymentRequest, BillPaymentRequestAdmin)
admin.site.register(Bill, BillAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(Mail, MailAdmin)
admin.site.register(SIMCard, SIMAdmin)
admin.site.register(EducationalReceipt, EducationalReceiptAdmin)
admin.site.register(Request, RequestAdmin)
admin.site.register(ActivatingSIMCardRequest, ActivatingSIMCardRequestAdmin)
admin.site.register(MinorConversationsRequest, MinorConversationsAdmin)
admin.site.register(ReceivingCarTollsRequest, ReceivingCarTollsAdmin)
admin.site.register(TrackingMailRequest, TrackingMailAdmin)
admin.site.register(ReceivingEducationalReceiptRequest, ReceivingEducationalReceiptAdmin)
