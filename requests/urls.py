from django.urls import path, include
from .views import *


urlpatterns = [
    path('signup/', SignupView.as_view(), name='signup'),
    path('login/', LoginView.as_view(), name='login'),
    path('activate-sim/', CreateActivatingSIMCardRequest.as_view(), name='activate_sim'),
    path('minor-conversations/', CreateMinorConversationsRequest.as_view(), name='minor_conversations'),
    path('car-tolls/', CreateReceiveCarTollsRequest.as_view(), name='car_tolls'),
    path('tracking-mails/', CreateTrackingMailRequest.as_view(), name='tracking_mails'),
    path('educational-receipts/', CreateReceivingEducationalReceiptRequest.as_view(), name='educational_receipts'),
    path('bill-payment/', CreateBillPaymentRequest.as_view(), name='bill_payment'),
    path('report-requests/', ReportRequests.as_view(), name='report_requests')
]
