from rest_framework import serializers
from .models import *


class BillPaymentRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = BillPaymentRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)


class EducationalReceiptRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()

    class Meta:
        model = ReceivingEducationalReceiptRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)

    def get_image(self, obj):
        request = self.context['request']
        return request.build_absolute_uri(obj.model.image.url)


class TrackingMailRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = TrackingMailRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)


class ReceiveCarTollsRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = ReceivingCarTollsRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)


class MinorConversationsRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = MinorConversationsRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)


class ActivatingSIMCardRequestSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = ActivatingSIMCardRequest
        fields = '__all__'

    def get_type(self, obj):
        return int(obj.type)

    def get_status(self, obj):
        return int(obj.status)
