from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from users.models import User
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from requests.models import *
from .serializers import *


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token)
    }


class SignupView(APIView):
    def post(self, request):
        data = request.data
        user = User.objects.create(
            first_name=data['first_name'],
            last_name=data['last_name'],
            national_code=data['national_code']
        )
        user.set_password(data['password'])
        user.save()
        return Response({
            'message': 'User was successfully created',
            'token': get_tokens_for_user(user)
        },
            status=status.HTTP_201_CREATED
        )


class LoginView(APIView):
    def post(self, request):
        data = request.data
        try:
            user = User.objects.get(national_code=data['national_code'])
        except User.DoesNotExist:
            return Response({
                'message': 'User with this national code does not exist'
            },
                status=status.HTTP_404_NOT_FOUND
            )
        if not user.check_password(data['password']):
            return Response({
                'message': 'Invalid password'
            },
                status=status.HTTP_401_UNAUTHORIZED
            )
        return Response({
            'message': 'User was logged in successfully',
            'token': get_tokens_for_user(user)
        },
            status=status.HTTP_200_OK
        )


class CreateActivatingSIMCardRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        sim_card = SIMCard.objects.create(
            number=data['number'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            national_code=data['national_code']
        )
        ActivatingSIMCardRequest.objects.create(
            model=sim_card,
            user=request.user,
            type=RequestChoices.ACTIVATING_SIM_CARD,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class CreateMinorConversationsRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        sim_card = SIMCard.objects.create(
            number=data['number'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            national_code=data['national_code']
        )
        MinorConversationsRequest.objects.create(
            model=sim_card,
            user=request.user,
            type=RequestChoices.MINOR_CONVERSATIONS,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class CreateReceiveCarTollsRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        car = Car.objects.create(
            chassis_number=data['chassis_number'],
            engine_number=data['engine_number'],
            plaque_number=data['plaque_number'],
            city_code=data['city_code']
        )
        ReceivingCarTollsRequest.objects.create(
            model=car,
            user=request.user,
            type=RequestChoices.CAR_TOLLS,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class CreateTrackingMailRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        mail = Mail.objects.create(
            tracking_code=data['tracking_code'],
            sender_name=data['sender_name'],
            receiver_name=data['receiver_name'],
            sender_postal_code=data['sender_postal_code'],
            receiver_postal_code=data['receiver_postal_code']
        )
        TrackingMailRequest.objects.create(
            model=mail,
            user=request.user,
            type=RequestChoices.TRACKING_MAIL,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class CreateReceivingEducationalReceiptRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        educational_receipt = EducationalReceipt.objects.create(
            image=request.FILES['image'],
            type=int(data['type']),
            first_name=data['first_name'],
            last_name=data['last_name'],
            national_code=data['national_code'],
            birthday=data['birthday']
        )
        ReceivingEducationalReceiptRequest.objects.create(
            model=educational_receipt,
            user=request.user,
            type=RequestChoices.RECEIVING_EDUCATIONAL_RECEIPT,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class CreateBillPaymentRequest(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        bill = Bill.objects.create(
            type=int(data['type']),
            cost=data['cost'],
            bill_id=data['bill_id'],
            payment_id=data['payment_id']
        )
        BillPaymentRequest.objects.create(
            model=bill,
            user=request.user,
            type=RequestChoices.BILL_PAYMENT,
            status=StatusChoices.PENDING
        )
        return Response({
            'message': 'Request was recorded successfully'
        },
            status=status.HTTP_201_CREATED
        )


class ReportRequests(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        bill_payment_requests = BillPaymentRequest.objects.filter(user=request.user)
        educational_receipt_requests = ReceivingEducationalReceiptRequest.objects.filter(user=request.user)
        tracking_mail_requests = TrackingMailRequest.objects.filter(user=request.user)
        car_tolls_requests = ReceivingCarTollsRequest.objects.filter(user=request.user)
        minor_conversations_requests = MinorConversationsRequest.objects.filter(user=request.user)
        activating_sim_card_requests = ActivatingSIMCardRequest.objects.filter(user=request.user)
        return Response({
            'bill_payment': BillPaymentRequestSerializer(
                bill_payment_requests, many=True, context={'request': request}
            ).data,
            'educational_receipt': EducationalReceiptRequestSerializer(
                educational_receipt_requests, many=True, context={'request': request}
            ).data,
            'tracking_mail': TrackingMailRequestSerializer(
                tracking_mail_requests, many=True, context={'request': request}
            ).data,
            'car_tolls': ReceiveCarTollsRequestSerializer(
                car_tolls_requests, many=True, context={'request': request}
            ).data,
            'minor_conversations': MinorConversationsRequestSerializer(
                minor_conversations_requests, many=True, context={'request': request}
            ).data,
            'activating_sim_card': ActivatingSIMCardRequestSerializer(
                activating_sim_card_requests, many=True, context={'request': request}
            ).data
        })
