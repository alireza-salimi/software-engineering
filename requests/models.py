from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator, MaxLengthValidator, MinLengthValidator
from users.models import User


class BillChoices(models.TextChoices):
    WATER = (1, _('Water'))
    ELECTRICITY = (2, _('Electricity'))
    GAS = (3, _('Gas'))


class EducationalReceiptChoices(models.TextChoices):
    HIGH_SCHOOL = (1, _('High school'))
    PRE_UNIVERSITY = (2, _('Pre-university'))


class RequestChoices(models.TextChoices):
    BILL_PAYMENT = (1, _('Bill payment'))
    TRACKING_MAIL = (2, _('Tracking mail'))
    ACTIVATING_SIM_CARD = (3, _('Activating SIMCard'))
    CAR_TOLLS = (4, _('Car tolls'))
    MINOR_CONVERSATIONS = (5, _('Minor conversation'))
    RECEIVING_EDUCATIONAL_RECEIPT = (6, _('Receiving educational receipt'))


class StatusChoices(models.TextChoices):
    PENDING = (1, _('Pending'))
    IN_PROGRESS = (2, _('In progress'))
    DONE = (3, _('Done'))


class Bill(models.Model):
    type = models.CharField(max_length=15, choices=BillChoices.choices, verbose_name=_('Type'))
    cost = models.FloatField(verbose_name=_('Cost'))
    bill_id = models.IntegerField(verbose_name=_('Bill ID'))
    payment_id = models.IntegerField(verbose_name=_('Payment ID'))

    class Meta:
        verbose_name = _('Bill')
        verbose_name_plural = _('Bills')

    def __str__(self):
        return f'Bill id: {str(self.bill_id)}'


class SIMCard(models.Model):
    number = models.CharField(max_length=11, verbose_name=_('Number'))
    first_name = models.CharField(max_length=150, verbose_name=_('Name'))
    last_name = models.CharField(max_length=150, verbose_name=_('Last name'))
    national_code = models.CharField(max_length=150, verbose_name=_('National code'))

    class Meta:
        verbose_name = _('SIMCard')
        verbose_name_plural = _('SIMCards')

    def __str__(self):
        return f'Number: {str(self.number)}'


class Car(models.Model):
    chassis_number = models.CharField(
        max_length=17,
        validators=[MaxLengthValidator(17), MinLengthValidator(17)], verbose_name=_('Chassis number')
    )
    engine_number = models.CharField(
        max_length=17,
        validators=[MaxLengthValidator(17), MinLengthValidator(17)], verbose_name=_('Engine number')
    )
    plaque_number = models.CharField(
        max_length=6,
        validators=[MaxLengthValidator(6), MinLengthValidator(6)], verbose_name=_('Plaque number')
    )
    city_code = models.IntegerField(
        validators=[MinValueValidator(10), MaxValueValidator(99)], verbose_name=_('City code')
    )

    class Meta:
        verbose_name = _('Car')
        verbose_name_plural = _('Cars')

    def __str__(self):
        return f'Chassis number: {self.chassis_number}'


class Mail(models.Model):
    tracking_code = models.CharField(
        max_length=13,
        validators=[MinLengthValidator(13), MaxLengthValidator(13)], verbose_name=_('Tracking code')
    )
    sender_name = models.CharField(max_length=150, verbose_name=_('Sender name'))
    receiver_name = models.CharField(max_length=150, verbose_name=_('Receiver name'))
    sender_postal_code = models.CharField(
        max_length=10,
        validators=[MinLengthValidator(10), MaxLengthValidator(10)], verbose_name=_('Sender postal code')
    )
    receiver_postal_code = models.CharField(
        max_length=10,
        validators=[MinLengthValidator(10), MaxLengthValidator(10)], verbose_name=_('Receiver postal code')
    )

    class Meta:
        verbose_name = _('Mail')
        verbose_name_plural = _('Mails')

    def __str__(self):
        return f'Tracking code: {self.tracking_code}'


class EducationalReceipt(models.Model):
    image = models.ImageField(upload_to='images/', verbose_name=_('Image'))
    type = models.CharField(max_length=15, choices=EducationalReceiptChoices.choices, verbose_name=_('Type'))
    first_name = models.CharField(max_length=150, verbose_name=_('Name'))
    last_name = models.CharField(max_length=150, verbose_name=_('Last name'))
    national_code = models.CharField(max_length=150, verbose_name=_('National code'))
    birthday = models.DateField(verbose_name=_('Birthday'))

    class Meta:
        verbose_name = _('Educational receipt')
        verbose_name_plural = _('Educational receipts')

    def __str__(self):
        return f'{self.first_name} {self.last_name}-{self.national_code}'


class Request(models.Model):
    type = models.CharField(max_length=20, choices=RequestChoices.choices, verbose_name=_('Type'))
    status = models.CharField(max_length=20, choices=StatusChoices.choices, verbose_name=_('Status'))
    date = models.DateField(auto_now=True, verbose_name=_('Date'))

    class Meta:
        verbose_name = _('Request')
        verbose_name_plural = _('Requests')

    def __str__(self):
        return f'{self.type}-{self.status}-{self.date}'


class BillPaymentRequest(Request):
    model = models.OneToOneField(to=Bill, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='bill_payment_requests'
    )

    class Meta:
        verbose_name = _('Bill payment request')
        verbose_name_plural = _('Bill payment requests')


class ActivatingSIMCardRequest(Request):
    model = models.OneToOneField(to=SIMCard, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='activating_sim_card_requests'
    )

    class Meta:
        verbose_name = _('Activating SIMCard request')
        verbose_name_plural = _('Activating SIMCard requests')

    def __str__(self):
        return super(ActivatingSIMCardRequest, self).__str__()


class MinorConversationsRequest(Request):
    model = models.OneToOneField(to=SIMCard, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='minor_conversations_requests'
    )

    class Meta:
        verbose_name = _('Minor conversations request')
        verbose_name_plural = _('Minor conversations requests')

    def __str__(self):
        return super(MinorConversationsRequest, self).__str__()


class ReceivingCarTollsRequest(Request):
    model = models.OneToOneField(to=Car, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='receiving_car_tolls_requests'
    )

    class Meta:
        verbose_name = _('Receiving car tolls request')
        verbose_name_plural = _('Receiving car tolls requests')

    def __str__(self):
        return super(ReceivingCarTollsRequest, self).__str__()


class TrackingMailRequest(Request):
    model = models.OneToOneField(to=Mail, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='tracking_mail_requests'
    )

    class Meta:
        verbose_name = _('Tracking mail request')
        verbose_name_plural = _('Tracking mail requests')

    def __str__(self):
        return super(TrackingMailRequest, self).__str__()


class ReceivingEducationalReceiptRequest(Request):
    model = models.OneToOneField(to=EducationalReceipt, on_delete=models.CASCADE, verbose_name=_('Model'))
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE, verbose_name=_('User'),
        related_name='receiving_educational_receipts_requests'
    )

    class Meta:
        verbose_name = _('Receiving educational receipt request')
        verbose_name_plural = _('Receiving educational receipt requests')

    def __str__(self):
        return super(ReceivingEducationalReceiptRequest, self).__str__()
