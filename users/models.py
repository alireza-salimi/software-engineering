from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, national_code, password=None, **extra_fields):
        if not national_code:
            raise ValueError('User should have a national code')
        if not password:
            raise ValueError('User should have a password')
        user = self.model(national_code=national_code, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, national_code, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')
        return self.create_user(national_code, password, **extra_fields)


class User(AbstractUser):
    national_code = models.CharField(
        max_length=10, unique=True, verbose_name=_('National code'), blank=False, null=False
    )
    last_name = models.CharField(max_length=150, blank=False, null=False, verbose_name=_('Last name'))
    USERNAME_FIELD = 'national_code'
    REQUIRED_FIELDS = ['first_name', 'last_name']
    username = None
    email = None
    objects = UserManager()

    def __str__(self):
        return str(self.first_name)
