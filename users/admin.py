from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from users.models import User


class CustomUserAdmin(UserAdmin):
    list_display = ['national_code', 'first_name', 'last_name']
    search_fields = ['national_code', 'first_name', 'last_name']
    ordering = ['date_joined']
    fieldsets = (
        (_('Personal information'), {
            'fields': ('first_name', 'last_name', 'national_code')
        }),
        (_('Date information'), {
            'fields': ('date_joined', 'last_login')
        }),
        (_('Other information'), {
            'fields': ('is_staff', 'is_active', 'is_superuser')
        })
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'national_code', 'password1', 'password2'),
        }),
    )


admin.site.register(User, CustomUserAdmin)
